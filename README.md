# WaterHCI

WaterHCI = Water-Human-Computer Interface

## Support
Please use the GitLab issue tracker for any issues. 

## Roadmap
We organize an annual confernence. You can view our news page for more: https://waterhci.com/news.php

## Contributing
We are open to contributions. 

## Authors and acknowledgment
Steve Mann - https://eyetap.org<br>


## License
WaterHCI follows a Copyleft approach with software licensed under a GNU General Public License v3.0

## Project status
Active. A conference is organized every year. 
